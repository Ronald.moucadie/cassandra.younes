## Journal de Bord

### Jeudi 1 Octobre 2020

Conférence très intéressante au matin sur le Bic, son historique, son design et son évolution. Victor et André nous ont ensuite parlé de leur vélo et de leur compositions très différentes, cela d'un aspect juridique aussi avec les éléments qui peuvent être remplacés.

En après-midi on est allé au musée pour voir les différents objets et en choisir un que l'on va devoir revisiter. J'ai choisi un fauteuil pliable et transportable que j'ai trouvé très intéressant pour cet aspect, il peut etre utilisé en extérieur et une fois plié il est résistant à l'eau. Son look inspiré de l'époque pop art avec une couleur flashy et brillante dû au plastique lui donne un aspect assez particulier qui contraste avec l'intérieur qui à l'air très chaud et confo. 

On a terminé par se présenter et présenter l'objet un par un.

### Jeudi 8 Octobre 2020

Aujourd'hui il y avait les formations git et Fusion 360 pour le module 1 et en module 3 on a fait un petit exercice pour débuter, on a pris un stylo et on a essayé de comprendre les choix du design. Pour la prochaine séance on s'est réparti plusieurs parties à approfondir, l'historique du stylo, l'influence culturelle, le design, la production ainsi que divers tests.

### Jeudi 15 Octobre 2020

On a continué les recherches sur l'objet choisi.

### Jeudi 22 Octobre 2020

Idem.

## Peter Ghyczy - Garden Egg Chair

![Peter](docs/images/Screen-Shot-2018-02-07-at-11.21.48-940x670.png)

<il><ul><li>**Designer**: Peter Ghyczy

<il><ul><li>**Entreprise**: Reuter Produkt Design / Elastogran, Allemagne

<il><ul><li>**Fonction**: Fauteuil

<il><ul><li>**Matériau**: Polyuréthane

<il><ul><li>**Date**: 1968

<il><ul><li>**Poids**: 12,5kg

### Peter Ghyczy

| Portrait | Présentation |
| --- | --- |
|![Peter](docs/images/Peter_Ghyczy2.jpg)|Peter Ghyczy a fuit la Hongrie pendant la révolution hongroise de 1956 pour déménager à Aix-la-Chapelle en Allemagne Ouest où il étudia l’ingénierie et l’architecture. C’est pendant ses études qu’il a développé une fascination pour la fonction plutôt que pour la forme “Je n’aime pas les illusions”. Après avoir terminé ses études, il commença à travailler chez Elastogran / Reuter, entreprise allemande où il dirigea le département design entre 1968 et 1972 en travaillant avec un nouveau matériau, le plastique. Ses premières expérimentations avec le plastique étaient pionnières, il tentait de les mêler à une approche fonctionnaliste notamment avec son produit emblématique, la *Garden Egg Chair*. L’objet fût surtout apprécié pour sa forme mais la motivation première de Ghyczy était pragmatique, avoir un meuble extérieur avec des coussins qui reste sec même lorsqu’il pleut. [Interview Peter Ghyczy RTBF Culture](https://www.rtbf.be/auvio/detail_peter-ghyczy-et-la-garden-egg-chair?id=2316303)|

### Reuter / Elastogran

En 1968, Gottfried Reuter de l'entreprise Reuter / Elastogran sollicite Ghyczy afin de créer un centre de design précisemment pour la promotion industrielle d'un produit, le polyuréthane. Le Groupe Elastogran à Lemförde est l'un des leaders mondiaux des polyuréthanes désormais sous le nom de *Polyuréthanes BASF GmbH* et membre du groupe BASF, groupe chimique allemand le plus grand au monde qui s'étend à travers le monde en Europe, Amérique du Nord et Asie Pacifique et qui couvre un grand nombres d'activités, produits pour l'agriculture, colorants, matières plastiques, produits pharmaceutiques, biotechnologie, pétrochimie de base, engrais, peintures, gaz et pétrole et produits de construction. En travaillant avec les ingénieurs de Reuter ils ont pu obtenir de bons résulats quant à l'épaisseur des coques pour les rendre plus légeres mais résistantes. A l'époque c'était très innovant de pouvoir travailler avec ce genre de matériaux, Peter Ghyczy était donc ravi de pouvoir diriger le département design.

Derrière l'histoire de la production de masse de la chaise se cache également une histoire fascinante sur les relations est-ouest en Allemagne. L'entreprise de Lemförde n'a produit que quelques chaises en testant le nouveau matériau qu'était le polyuréthane. Cependant, l'entreprise a décidé de produire en série la chaise en Allemagne de l'Est car la production y était beaucoup moins chère. Les échanges industriels entre l'Allemagne de l'Ouest capitaliste et l'Allemagne de l'Est socialiste n'ont pas été reconnus. La licence de production de la chaise a été vendue au VEB Synthesewerk Schwarzheide près de la ville de Senftenberg.

### Contexte

#### Epoque pop

C’est au **début des années 60**, en Angleterre et aux Etats-Unis que se propage un nouveau mouvement flamboyant, **le design pop**. Principalement révélé à l’aide de l’image colorée d’**Andy Warhol**, il s’inspire de la bande dessinée et se démarque par ses **couleurs vives et ses formes pleines**. Les designs sont travaillés en prenant en compte l’aspect **graphique** de l’affichage et de la publicité, dans une optique de **consommation de masse**. Cela marque une certaine **révolution du goût populaire**.

A travers le design pop une volonté d’**anti conventionnalisme** se ressent par un idéal de design **anti fonctionnaliste** dans plusieurs domaines, la musique, l’attitude, les vêtements. Cet élan est perçu par certains designers comme une renaissance du design. 

La génération visée par cette commercialisation est celle du baby boom, ce design n’est pas un style, c’est un réel **mouvement éphémère et variable**. De nombreuses images de symboles caractérisent cette nouvelle tendance du design qui revendique l’**arbitraire**. Cela s’observe dans la mode aussi avec la minijupe et le blue jean qui incitent un **changement de style et d’attitude**, on ne s’assied plus de la même façon.

C’est l’ère du **jetable**, des objets plein d’astuces et **bon marché**. Certains designers dessinent des robes jetables en papier illustré. Les produits deviennent obsolètes, processus favorisé par la pub et le marketing. Les objets sont conçus pour **séduire et susciter la demande** plus que pour répondre à un besoin. Et cela touche notamment le secteur des meubles qui était considéré comme **durable**, avec l'avènement du carton et du PVC gonflable.

#### Le plastique

Les réponses de l’industrie du meuble afin de satisfaire les goûts des enfants du baby-boom et répondre à une production en **grande quantité** se matérialisent en fournissants des produits **peu coûteux, mobiles, légers et colorés**. A l’époque le coût du **pétrole** était moindre et les designers s’orientent vers le **plastique**, principalement l’ABS (plastique rigide),le polyéthylène et d’autres thermoplastiques. Leurs qualités, ils sont **légers, résistants, colorés et brillants**.

La matière première avait beau être bon marché, le **moule lui était coûteux** ce qui pousse la **production de masse** pour rentabiliser le coût du moule et permettre au produit d’avoir un prix abordable. Il s’écoule rapidement et est remplacé par un autre produit différent. C’est la **mode changeante**.

#### Aspect pratique

De nouveaux besoins se font ressentir spécialement dans les hôpitaux, les administrations et les hôtels, ce qui crée des changements de design. Les meubles doivent être **transportables facilement, aisément manipulables et bon marché**.

Les concepts de **flexibilité et de modularité** se développent pour combiner plusieurs fonctions. 

### Idée du projet et innovation

![o](docs/images/GHYCZY_atelier3.jpg)

C’est dans ce contexte qu’est née l’idée de base de ce projet, créer un fauteuil qui pourrait être utilisé tant en intérieur qu’en extérieur mais surtout qui pourrait passer la nuit dehors sans être endommagé par la pluie. 

Pour cela, le designer Peter Ghyczy a imaginé un fauteuil qui se déploie et peut être fermé à volonté et même utilisé en tant qu’assise ainsi. Cet objet est très représentatif de l’époque et des visions progressistes et utopiques qui dominaient dans les conceptions contemporaines de part sa forme futuriste ovni, sa portabilité et sa fonction de prélassement informel. 

Son aspect extérieur a aussi été travaillé comme une sculpture d'extérieur en forme d’ovni qui peut rester la tout le temps.

![](docs/images/view.jpg)

### Design

#### Matériau / Fabrication

Ghyczy travaille avec l'entreprise Elastogran qui emploie principalement le polyuréthane, un matériau découvert en 1937 et connu pour sa polyvalence, sa resistance et sa durabilité. Son utilisation est très varié et couvre divers domaines, au sein du secteur automobile il est employé de plusieurs manières principalement pour les sièges en tant que mousse souple moulée et pour les ailerons, composés des pares-chocs avants, arrières et latéraux pour sa variété de formes, sa légèreté, sa diversité de finitions et son assemblage facile. Ce matériau profère aux ailerons qui doivent survivre en mileu extérieur une grande resistance au rayonnement solaire, aux températures extrêmes et à la pluie.

Lorsqu'il est versé dans un moule métallique, le polyuréthane se solidifie en une masse poreuse ressemblant à de l'os. Il est peu coûteux, léger, presque incassable et peut prendre pratiquement n'importe quelle forme à l'aide d'un moule. Voici un exemple de [fabrication dans le secteur automobile](https://www.youtube.com/watch?v=mXBGFkaQLes&ab_channel=YongjiaPolyurethane).

![](docs/images/10663503_bukobject.jpg)

Une fois moulé le polyuréthane permet à la Garden Egg Chair de se présenter sous plusieurs aspects avec les différentes couleurs flashy.

![](docs/images/viewjj.jpg)

#### Formes et fonctions

La fonction de base de la Garden Egg chair est de pouvoir se prélasser en extérieur ce qui cadre le design avec certaines contraintes. L'un des premiers critères qui entre en jeu lorsqu'on parle d'un fauteuil mis à part les questions techniques c'est bien entendu son confort, le matériau sur lequel on s'assoit est-il confortable? La hauteur ainsi que la position des jambes est-elle adéquate? De même pour la position du dos et de la tête. 

Ghyzcy voulait combiner le confort intérieur que l'on retrouve grâce aux coussins sur les fauteuils avec l'extérieur tout en ayant pas plus qu'à refermer un couvercle pour les protéger des intemperies et éviter de devoir tout débarasser de l'extérieur. 

Comme il s'agit principalement d'un fauteuil à usage externe on ne s'assoit pas de la même manière qu'en intérieur, on a tendance à être plus bas comme pour les tables basses en intérieur, on a les jambes plus tendues et le dos repose en arrière.

![](docs/images/541521.JPG)
Neufert

Même si la forme de base de l'oeuf écrasé n'est pas venue tout de suite, le design de la forme intègre des caractéristiques typiques de l'époque: un look de la conquête spatiale, une forme semblable à un ovni, un plastique laqué de couleur vive, la portabilité et la fonction de détente informelle du siège bas. 

Au départ le design de l'objet ressemblait à une valise pour ensuite évoluer en ce qu'il est aujourd'hui.

![](docs/images/123984771_3474461099312805_3494180330028584919_n.jpg)

![](docs/images/dimensions.jpg)

Cependant l'assise ici est encore plus basse que la norme et le dossier n'est pas énormement incliné. Cela laisse penser que ce fauteuil est plus adapté à des personnes plus petites ou à des enfants pour éviter l'inconfort, même si eux aussi semblent avoir adapté leur manière de s'assoir à l'objet.

![](docs/images/ghyczy_egg_chair_sweden_1985_custom_290x194_06200416.jpg)

Outre le confort, ce fauteuil fait face à d'autres contraintes liées à sa portabilité et le contexte dans lequel il doit survivre. Le matériau utilisé pour la coque résiste à la pluie mais il existe de nombreuses failles et jonctions nécessaires à l'aspect pliable et dépliable de l'objet, ces zones la sont des zones qui risquent de faire rentrer de l'eau à l'intérieur, c'est pour cela qu'elles ont été travaillées minutieusement à l'aide de creux qui ne laissent pas passer l'eau et les jointures ont été travaillées telles les jointures dans l'automobile au niveau des portes.

J'ai souhaité passer par le dessin pour essayer de mieux comprendre les éléments qui composent cet objet qui semble cacher des détails ingénieux.

![](docs/images/schema123.jpg)

![](docs/images/schema456.jpg)

![](docs/images/coupe.jpg)
Tentative coupe à la main.

### Réception du projet

La Garden Egg Chair fût fabriquée pendant deux à trois ans avant l’interruption de la production dûe à la complexité dans le processus de laquage et à l’échec en terme de vente. Le prix était beaucoup trop élevé et malgré le succès et l’engouement autour de cet objet pour son pragmatisme et ses fonctionnalités les ventes ne le reflètent pas.  

Peter Ghyczy quitta l’Allemagne en 1972 et fonda sa propre entreprise aux Pays-Bas. En 2001 la production d’une version améliorée de la Garden Egg Chair fût lancée par l’entreprise avec en plus une gamme de couleurs interchangeables pour le siège et la coque.
Mais ce fût encore un échec au niveau des ventes, à part un achat notable de Karl Lagerfeld. La production fût donc de nouveau interrompue.

"En plus, le moule qui sert à le fabriquer est presque cassé. Il faudrait plus de 150.000 euros pour en faire un nouveau. Cet investissement, je ne veux pas le faire. L'oeuf était notre produit phare, mais, hélas, nous n'en tirons rien au niveau commercial. La production de la réédition s'arrêtera donc définitivement. Celui qui en veut une devra acheter un exemplaire vintage. Il y en a suffisamment." Ghyzcy

### Mercredi 28 Octobre 2020

Première tentative de contact avec l'architecte et designer Peter Ghyczy afin d'avoir plus d'informations concernant le moule et la partie cachée et évidée de l'objet.

![](docs/images/hk.JPG)

En regardant sur le site de Ghyczy, il y avait une liste des différents showrooms qui disposent ses objets aux Pays-Bas, à Londres et en Belgique et parmi eux il y en avait un à Bruxelles, à Uccle. L'après-midi je suis donc allée avec Fatima voir ce showroom chez Dominique Rigo qui est en fait une entreprise de design d'intérieur comprenant une équipe de sept architectes d'intérieur et 3 showrooms de mobilier moderne offrant une grande sélection de mobilier contemporain à Bruxelles.

C'était vraiment chouette de pouvoir sortir du cadre musée où l'on ne peut qu'interagir avec le regard et pouvoir toucher l'objet, sentir la matière et surtout tester ses fonctionnalités.

| Photo 1 | Photo 2 | Photo 3 | Photo 4 |
| --- | --- | --- | --- |
| ![](docs/images/122891270_1239055996429086_4272680523047423903_n.jpg) | ![](docs/images/122879023_423570561982754_3704740616259045419_n.jpg) | ![](docs/images/123073580_419744159011944_8244792645328957752_n.jpg) | ![](docs/images/122880782_366775481204782_4573956231941746623_n.jpg) |
| C'était intéressant de pouvoir observer la Garden Egg Chair dans une autre couleur, en blanc ça fait tout de suite plus sobre et moins flashy mais l'aspect plastique se voit fortement. | J'ai pu tourné l'objet dans tous les sens et j'ai relevé la poignée pour le porter à l'arrière. | Le dessous de l'objet était refermé avec une plaque donc on ne voit pas l'intérieur évidé comme sur certaines photos. | J'ai remarqué qu'à l'arrière au niveau de la jonction entre le couvercle et le siège il y avait un interstice donc l'eau peut s'y introduire. |

| Photo 5 | Photo 6 | Photo 7 | Photo 8 |
| --- | --- | --- | --- |
| ![](docs/images/122860994_407245850282527_2076546293068988801_n.jpg) | ![](docs/images/122873750_3679720475424255_1317973953192678819_n.jpg) | ![](docs/images/122873248_648635746013996_4792492147833968319_n.jpg) | ![](docs/images/122993650_720782651980221_694961520838721605_n.jpg) |
| Je trouve que cet objet fermé a un aspect mystérieux comme une petite boite à bijoux, on a envie de voir ce qui s'y cache. | Le couvercle vient simplement se poser sur la partie basse donc il n'y a pas de résistance pour l'ouvrir. | L'intérieur chaud grâce à la texture du coussin semble réelement en contraste avec l'extérieur d'aspect froid à cause du plastique. | Une fois déployé, il reste de petite taille et fait plus penser à un fauteuil pour enfant. |

| Photo 9 | Photo 10 | Photo 11 | Photo 12 |
| --- | --- | --- | --- |
| ![](docs/images/122977828_366467111076714_1997020549047223542_n.jpg) | ![](docs/images/122907209_274084843911668_1571633078450400018_n.jpg) | ![](docs/images/122893410_683073919078824_6276807897324859054_n.jpg) | ![](docs/images/122907212_272512850772184_4993988008102243007_n.jpg) |
| A l'arrière, au niveau de l'interstice on voit qu'il y a un creux pour que le couvercle puisse tourner mais ça permet aussi de recolter l'eau sans qu'elle touche l'arrière du coussin, il y a trois petits trous surement pour l'écoulement de l'eau qui ressort ensuite par le bas qui est aussi perforé. | En enlevant la partie haute du coussin on peut observer le système métallique qui permet de faire tourner le couvercle et aussi les scratchs qui permettent de maintenir le coussin. | En enlevant la partie basse du coussin on peut voir que le siège s'abaisse à l'arrière, il y a une différence de 15 cm entre l'avant et l'arrière de l'assise. | Petit détail sans trop d'importance, les coussins ont leur petite étiquette signé Ghyczy et ils font 5 cm d'épaisseur, le plastique lui ne fait que 2 cm d'épaisseur. |

| Photo 13 | Photo 14 | Photo 15 | Photo 16 |
| --- | --- | --- | --- |
| ![](docs/images/122891830_3692494757449352_6382374748326451394_n.jpg) | ![](docs/images/122954498_293939598293693_400769276521485536_n.jpg) | ![](docs/images/122892042_727035918159331_6149228743610735305_n.jpg) | ![](docs/images/122904685_2451413398495958_3358953238529536155_n.jpg) |
| J'ai essayé de le soulever et ça a été un échec pour moi, impossible de le soulever à une main et impossible de le tenir à l'autre extrémité par le bas vu que je n'ai pas des bras assez long, de plus comme le couvercle n'est pas vraiment fixé il a tendance à s'ouvrir quand on penche l'objet. | Après l'effort le réconfort, j'ai pu m'assoir les jambes tendues et profiter des qualités des sièges bas. J'ai trouvé le coussin particulièremet confortabe surtout pour la partie basse du corps et la hauteur est adéquate pour les jambes tendues. | Par contre au niveau du dos et de la tête je n'ai pas trouvé l'inclinaison très agréable même s'il ne semble pas y avoir de risque de bascule. Ma tête reposait tout juste sur le haut du coussin du coup je me disais que pour une personne plus grande c'est peut être désagréable parce que le tête ne reposerait plus sur le coussin. | J'ai aussi testé jambes pliées et j'ai trouvé ça limite, pour moi c'était encore assez confortable mais pour une personne plus grande l'angle au niveau des genoux doit être inconfortable. |

| Photo 17 | Photo 18 | Photo 19 | Photo 20 |
| --- | --- | --- | --- |
| ![](docs/images/122873248_2645163855794451_4747303110641798013_n.jpg) | ![](docs/images/122893690_362239221785099_443754275231546192_n.jpg) | ![](docs/images/122906105_689981864961077_6528899834850541567_n.jpg) | ![](docs/images/122921016_717732288826213_4268810344232784986_n.jpg) |
| Pour finir j'ai voulu m'assoir au dessus vu qu'il peut sois disant être employé de la sorte. D'abbord dans un certain sens mais l'objet basculait d'avant en arrière et l'assise n'était pas très stable j'ai donc du écarter les jambes pour plus de stabilité. | Mais le plastique avec sa surface super lisse et laquée eh ben ça glisse! | Dans ce sens l'objet ne basculait plus donc je pouvais me poser avec les jambes peu écartées. | Mais bon ça reste le même matériau de finitions dans ce sens et ça glisse! |